#!/bin/sh

USER_NAME=${USER_NAME:-bastion}

# Add user
echo "Add $USER_NAME user"
adduser -D $USER_NAME -s /bin/ash
sed -i "s/${USER_NAME}:!/${USER_NAME}:*/g" /etc/shadow

# Set up SSH directory permissions
SSH_DIR="/home/$USER_NAME/.ssh"
mkdir -p "$SSH_DIR"
chmod 700 "$SSH_DIR"
chown -R $USER_NAME:$USER_NAME "$SSH_DIR"

# Setting security for SSH
update_sshd_config() {
    setting=$1
    value=$2
    sed -i "/^#$setting/c\\$setting $value" /etc/ssh/sshd_config
    sed -i "/^$setting/c\\$setting $value" /etc/ssh/sshd_config
}

update_sshd_config "PasswordAuthentication" "no"
update_sshd_config "PermitEmptyPasswords" "no"
update_sshd_config "PermitRootLogin" "no"
echo "User/password ssh access is disabled."

# Allow TCP forwarding based on env var
FORWARDING_STATUS="enabled"
if [ "$ALLOW_TCP_FORWARDING" = "false" ]; then
    update_sshd_config "AllowTcpForwarding" "no"
    FORWARDING_STATUS="disabled"
else
    update_sshd_config "AllowTcpForwarding" "yes"
fi
echo "Forwarding is $FORWARDING_STATUS."

# Add public key if provided
AUTH_FILE="$SSH_DIR/authorized_keys"
[ -f "$AUTH_FILE" ] || touch "$AUTH_FILE"
if [ -n "$PUBLIC_KEY" ] && ! grep -q "${PUBLIC_KEY}" "$AUTH_FILE"; then
    echo "$PUBLIC_KEY" >> "$AUTH_FILE"
    chmod 600 "$AUTH_FILE"
    chown $USER_NAME:$USER_NAME "$AUTH_FILE"
    echo "Public key from env variable added."
fi

# Generate host keys if not existing
if [ ! -f "/etc/ssh/ssh_host_rsa_key" ]; then
    echo "Generating host keys"
    /usr/bin/ssh-keygen -A
fi

# Run SSHD in the foreground
/usr/sbin/sshd -D -e -4 -f /etc/ssh/sshd_config
