# **PodBastion**

## **Overview**

`PodBastion` provides a convenient means to deploy temporary bastion hosts within a Kubernetes cluster. A bastion, or bastion host, is a special instance designed to act as a singular entry point to a network (or cluster) from an unsecured network, such as the internet. Using a bastion minimizes the attack surface and enhances the security of your infrastructure.

This guide elucidates the process of utilizing `PodBastion` to establish a temporary bastion inside your Kubernetes cluster.

## **Prerequisites**

- `kubectl` installed on your computer.
- Access to a Kubernetes cluster. If utilizing an Amazon EKS cluster, ensure your `kubeconfig` is correctly set up.
- An SSH key pair (private and public keys). If absent, they can be generated using `ssh-keygen` on your machine.

## **Environment Variables**

When booting up the pod, the following environment variables can be set to customize the bastion's behavior:

| Variable              | Default Value     | Description                                                                                        |
|-----------------------|-------------------|----------------------------------------------------------------------------------------------------|
| `USER_NAME`           | `bastion`         | Username for which SSH access is configured.                                                       |
| `ALLOW_TCP_FORWARDING`| Not set (true by default) | When set to "false", TCP forwarding will be disabled. Otherwise, it's enabled.        |
| `PUBLIC_KEY`          | None              | Public key to be appended to the authorized keys for SSH access. This is essential for SSH connection. |

To use these variables while launching your pod, add `--env` parameters for each variable you wish to define, as shown:

```bash
kubectl run bastion \
  --env="USER_NAME=myuser" \
  --env="PUBLIC_KEY=MY_PUBLIC_KEY" \
  --port=22 \
  --restart=Never \
  --image=registry.gitlab.com/maltyxx/podbastion:latest
```

## **Steps**

1. **Launching the Bastion**:
   Open your terminal (CMD on Windows, Terminal on MacOS or Linux).
   Start the bastion in your cluster using the Docker image from our registry at `registry.gitlab.com/maltyxx/podbastion`. Replace `MY_PUBLIC_KEY` with your SSH public key:
    ```bash
    kubectl run bastion \
      --env="PUBLIC_KEY=MY_PUBLIC_KEY" \
      --port=22 \
      --restart=Never \
      --image=registry.gitlab.com/maltyxx/podbastion:latest
    ```
   This creates a pod named "bastion" in the "default" namespace. The pod operates an OpenSSH server on port 22, and your public key is added to the authorized keys list for SSH connectivity.

2. **Setting up Port-Forwarding**:
   Organize port-forwarding for the "bastion" pod. This allows your local machine to communicate with the "bastion" pod via SSH:
    ```bash
    kubectl port-forward pod/bastion 2222:22
    ```

3. **SSH into the Bastion**:
   Engage with the bastion using SSH.
    ```bash
    ssh bastion@127.0.0.1 -p 2222
    ```
   Following this, you should be within the bastion pod. From here, you can access other cluster resources.

4. **Create an SSH Tunnel**:
   Should you require access to other cluster services from your local machine, establish an SSH tunnel. Replace `LOCAL_PORT`, `HOSTNAME`, and `PORT` with the service's appropriate values:
    ```bash
    ssh -N -L LOCAL_PORT:HOSTNAME:PORT bastion@127.0.0.1 -p 2222
    ```
   For instance, if you operate a database service at `db-service.default.svc.cluster.local:5432` and wish to access this service on your local machine's port `15432`:
    ```bash
    ssh -N -L 15432:db-service.default.svc.cluster.local:5432 bastion@127.0.0.1 -p 2222
    ```

5. **Cleaning up**:
   Once the bastion's utility is exhausted, it's prudent to remove it:
    ```bash
    kubectl delete pod bastion
    ```

## **Resources**

For additional insights on `kubectl` usage and Kubernetes Pod management, consult the [official Kubernetes documentation](https://kubernetes.io/docs/).

For the source code and updates, refer to [PodBastion on GitLab](https://gitlab.com/maltyxx/podbastion).
