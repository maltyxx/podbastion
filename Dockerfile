# Use a specific Alpine image to ensure reproducibility
FROM alpine:3.18 AS base

# Update the image and install necessary packages
FROM base AS builder

# Label the image with maintainer information
LABEL maintainer="Yoann Vanitou <yvanitou@gmail.com>" \
      description="Temporary SSH Bastion for Kubernetes using Alpine"

# Simplify package installation and remove unnecessary caches
RUN set -eux; \
    apk --no-cache add openssh-server; \
    rm -rf /var/cache/apk/* /tmp/*

# Expose the standard SSH port
EXPOSE 22/tcp

# Copy the necessary files into the image
COPY rootfs/ /

# Set the entry point to execute the script when the container starts
ENTRYPOINT ["/entrypoint.sh"]
